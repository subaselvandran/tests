app.controller("directiveController",function($scope,$window){
    $scope.property = [];
    $scope.flag=false;
    $scope.save = function(){
        saveFunc();
    }    
    $scope.load = function(){
        loadFunc();
    }   
    $scope.reset = function(){
        resetFunc();
    }   
    $scope.undo = function(){
        undoFunc();
    }   
    $scope.build = function(){
        var fileName = $window.prompt("Enter the saved-name to build");
        var readData;
        fs.readFile("app/config/domFile-" + fileName + ".json",function(err,data){
            if(err){
                $window.alert("Enter the correct name");
            }
            else{
                readData =  JSON.parse(data);
                var html = document.createElement("html");
                var body = document.createElement("body");
                readData.forEach(function(obj){
                    var ele = document.createElement(obj.tagName);
                    ele.id = obj.id;
                    ele.style.position = obj.position 
                    ele.style.top = obj.top;
                    ele.style.left = obj.left;
                    ele.type = obj.type;
                    ele.value = obj.value;
                    body.appendChild(ele);                     
                })
                html.appendChild(body);
                fs.writeFile("../../assignment-master/testApp/app/view//result.html",html.outerHTML);
            } 
        })
    }
    $scope.changeFunc = function(){
        $scope.flag=true;
    }
    $scope.saveProp = function(){
        console.log($scope.property)
        console.log($scope.jsonFileInput)
        $scope.jsonFileInput.forEach(function(element){
            if($scope.property.id == element.id){
                if($scope.property.Type != element.type){
                    element.type = $scope.property.Type;
                }
                if($scope.property.Value != element.value){
                    element.value = $scope.property.Value;
                }
                element.id = $scope.property.Id;
            }
        })
        displayDOM($scope.jsonFileInput);
        $scope.flag = false;
    }
    
});